#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
#  # @author: Arnaud Joset
#
#  This file is part of scikit-learn_for_linuxfr.
#
# scikit-learn_for_linuxfr is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# scikit-learn_for_linuxfr is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with scikit-learn_for_linuxfr.  If not, see <http://www.gnu.org/licenses/>.
#

import feedparser
import re
import csv
import logging

logging.basicConfig()
logger = logging.getLogger("linuxfr_atom2csv")
logger.setLevel("DEBUG")


class LinuxfrPredictor:
    def __init__(self):
        self.known_url = []
        self.tag_re = re.compile(r'<[^>]+>')

    def get_feeds(self):
        """
        Obtain atom feeds
        :return: a tabular with the content of the feeds
        """
        feed = feedparser.parse('http://linuxfr.org/journaux.atom')
        print("Feed: {}".format(feed))
        to_add = []
        for entry in feed.entries:
            to_add.append(entry)
        return to_add

    def remove_tags(self, input_html):
        """
        Remove HTML tag in string
        :param input_html: HTML string
        :return: clear text
        """
        return self.tag_re.sub('', input_html)

    def extract_infos(self, count, feed):
        """
        Get the desired information of the feeds: title, url, content, author name and the score of the publication
        :param count: 
        :param feed: the feed from feedparser
        :return: a dictionary containing the information
        """
        boundaries = [-50, 0, 50]


        scores = {
            'https://linuxfr.org/users/patrick32/journaux/rem-on-saura-peut-etre-faire-le-cafe-et-pas-vous-ficher-dehors':-7,
            'https://linuxfr.org/users/lawless/journaux/l-equipe-ubuntu-desktop-aimerait-avoir-vos-commentaires': 18,
            'https://linuxfr.org/users/rydroid/journaux/sortie-de-replicant-6-0': 21,
            'https://linuxfr.org/users/eaufroide/journaux/retour-d-experience-yunohost': 23,
            'https://linuxfr.org/users/sebastien_p/journaux/nouvelles-distributions-a-venir-sous-windows-10': 31,
            'https://linuxfr.org/users/faya/journaux/2-bookmarks-securite-windows': 30,
            'https://linuxfr.org/users/fantome_asthmatique/journaux/tres_hs-moyen-de-gamme-sur-lemonde-fr-nan-mais-je-reve': 4,
            'https://linuxfr.org/users/swilmet/journaux/kickstart-et-ansible-pour-automatiser-des-installations-configurations-de-systemes-linux': 25,
            'https://linuxfr.org/users/tilk/journaux/jouer-sous-linux-la-serie-des-shadowrun': 23,
            'https://linuxfr.org/users/guanglier/journaux/c2550d4i-et-marvell-88se9230': 10,
            'https://linuxfr.org/users/blackknight/journaux/un-decalage-de-64-bits-ca-vous-inspire-comment': 42,
            'https://linuxfr.org/users/regis/journaux/pinebook-opensource-notebook': 9,
            'https://linuxfr.org/users/yazgoo/journaux/presentation-du-multiplexer-de-sessions-ssh-cssh_tmux': 20,
            'https://linuxfr.org/users/wilk/journaux/hebergement-postgresql-en-ligne': 23,
            'https://linuxfr.org/users/gouttegd/journaux/de-la-distribution-des-clefs-openpgp': 51,
            'https://linuxfr.org/users/spacefox/journaux/kotlin-brainfuck-efficacite-compacite-optimisation': 40,
            'https://linuxfr.org/users/cyprien1/journaux/comment-je-suis-passe-d-ubuntu-a-debian-sid': 28,
            'https://linuxfr.org/users/denisdordoigne/journaux/pint-of-science-2017-c-est-la-semaine-prochaine': 20}

        param_feed = {'title': feed['title'], 'url': feed['link'],
                      'content': self.remove_tags(feed['content'][0]['value']).replace('\n', ' '),
                      'author': feed['author'], 'count':count}
        try:
            param_feed['score'] = scores[feed['link']]
        except KeyError:
            param_feed['score'] = 0
        if param_feed['score'] < min(boundaries):
            param_feed['quality_content'] = 'Magnificient Troll'
        if param_feed['score'] > max(boundaries):
            param_feed['quality_content'] = 'Quality Troll'
        if 0 < param_feed['score'] < max(boundaries):
            param_feed['quality_content'] = 'Average Troll'
        if 0 > param_feed['score'] > min(boundaries):
            param_feed['quality_content'] = 'Great Troll'




        return param_feed

    def open_csv(self):
        """
        Open a csv file cont
        :return: 
        """
        try:
            with open('linuxfr.csv', 'r') as csvfile:
                saved_feeds = csv.DictReader(csvfile, delimiter='£', quotechar='µ')
                for row in saved_feeds:
                    try:
                        self.known_url.append(row['url'])
                    except KeyError:
                        logger.debug("KeyError : key does not exist")
                        pass
            return True
        except FileNotFoundError:
            logger.error("CSV Python file not found")
            return False

    def save_csv(self, data):
        fieldnames = ['title', 'author', 'url', 'score', 'content', 'quality_content', 'count']
        with open('linuxfr.csv', 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter='£', quotechar='µ')
            writer.writeheader()
            for row in data:
                if row['url'] not in self.known_url:
                    logger.debug("Append url {} to file".format(row['url']))
                    writer.writerow(row)
                else:
                    logger.debug("URL {} already in the CSV file.".format(row['url']))


if __name__ == "__main__":
    lp = LinuxfrPredictor()
    ret = lp.get_feeds()
    data = []
    count = 0
    for feed in ret:
        param_feed = lp.extract_infos(count, feed)
        data.append(param_feed)
        count += 1
    ret = lp.open_csv()
    print(lp.known_url)
    lp.save_csv(data)
